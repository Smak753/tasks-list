package com.example.spisok_del

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_add.*


class Add : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add)
        var intent:Intent
        add.setOnClickListener(View.OnClickListener {
            if(new_text.text.toString()==""){
                Toast.makeText(applicationContext,getString(R.string.InputText),Toast.LENGTH_SHORT).show()
            }
            else
            {
            MyDatabaseOpenHelper.getInstance(applicationContext).addNotice(new_text.text.toString(),0)
            intent = Intent(applicationContext, MainActivity::class.java)
            startActivity(intent)
            }

        })
    }
}
