package com.example.spisok_del

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_change.*


class Change : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change)
        var Notice: Text = intent.getParcelableExtra("Notice")
        if(intent.getIntExtra("btn",2)==0) {
            delete.visibility = View.INVISIBLE
            change.visibility = View.INVISIBLE
            ok.setBackgroundResource(R.color.holoRedDark)
            ok.text =getString(R.string.delete_btn)
        }
        editView.setText(Notice.text)
        delete.setOnClickListener (View.OnClickListener {
            MyDatabaseOpenHelper.getInstance(applicationContext).deleteNotice(Notice.id)
            intent = Intent(applicationContext, MainActivity::class.java)
            startActivity(intent)
        })
        change.setOnClickListener (View.OnClickListener {
            if(editView.text.toString()==""){
                Toast.makeText(applicationContext,getString(R.string.InputText), Toast.LENGTH_SHORT).show()
            }
            else {
                MyDatabaseOpenHelper.getInstance(applicationContext).updateNotice(Notice.id, editView.text.toString(), 0)
                intent = Intent(applicationContext, MainActivity::class.java)
                startActivity(intent)
            }
        })
        ok.setOnClickListener (View.OnClickListener {
            if(intent.getIntExtra("btn",2)==0) {
                MyDatabaseOpenHelper.getInstance(applicationContext).deleteNotice(Notice.id)
            }else{
            MyDatabaseOpenHelper.getInstance(applicationContext).updateNotice(Notice.id,Notice.text,1)}
            intent = Intent(applicationContext, MainActivity::class.java)
            startActivity(intent)
        })
    }
}
