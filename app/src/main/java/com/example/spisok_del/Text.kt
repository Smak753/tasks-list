package com.example.spisok_del

import android.os.Parcel
import android.os.Parcelable


class Text() :Parcelable{
    var id:Int = 0
    var text:String = ""
    var phase:Int = 0

    constructor(parcel: Parcel) : this() {
        id = parcel.readInt()
        text = parcel.readString()
        phase = parcel.readInt()
    }

    override   fun toString(): String {
        return "Contact(id=$id,text=$text,phase=$phase"
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(text)
        parcel.writeInt(phase)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Text> {
        override fun createFromParcel(parcel: Parcel): Text {
            return Text(parcel)
        }

        override fun newArray(size: Int): Array<Text?> {
            return arrayOfNulls(size)
        }
    }
}