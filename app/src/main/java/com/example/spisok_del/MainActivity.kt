package com.example.spisok_del

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*


class MainActivity : AppCompatActivity() {
   override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        tabHost.setup()
        var tabSpec = tabHost.newTabSpec("tag1")
        tabSpec.setContent(R.id.tab1)
        tabSpec.setIndicator(getString(R.string.ActiveTasks))
        tabHost.addTab(tabSpec)
        tabSpec = tabHost.newTabSpec("tag2")
        tabSpec.setContent(R.id.tab2)
        tabSpec.setIndicator(getString(R.string.DoneTasks))
        tabHost.addTab(tabSpec)
        tabHost.currentTab = 0

        var intent:Intent




       list1.layoutManager =LinearLayoutManager(applicationContext)
       list1.adapter = TextAdapter(this@MainActivity,0)
       list2.layoutManager =LinearLayoutManager(applicationContext)
       list2.adapter = TextAdapter(this@MainActivity,1)




       fab.setOnClickListener (View.OnClickListener {
           intent = Intent(applicationContext, Add::class.java)
           startActivity(intent)
       })


    }
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
    }
