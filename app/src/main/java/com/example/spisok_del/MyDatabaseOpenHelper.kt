package com.example.spisok_del

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import org.jetbrains.anko.db.*

class MyDatabaseOpenHelper(ctx: Context) : ManagedSQLiteOpenHelper(ctx, "MyDatabase", null, 1) {

    var NOTICE_TABLE="Text"
    var ID="id"
    var TEXT="text"
    var PHASE="phase"

    companion object {
        private var instance: MyDatabaseOpenHelper? = null

        @Synchronized
        fun getInstance(ctx: Context): MyDatabaseOpenHelper {
            if (instance == null) {
                instance = MyDatabaseOpenHelper(ctx.getApplicationContext())
            }
            return instance!!
        }
    }

    var createSql:String="create table $NOTICE_TABLE($ID INTEGER PRIMARY KEY AUTOINCREMENT, $TEXT TEXT, $PHASE INTEGER)"

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(createSql)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {

    }

    fun addNotice(text:String,phase:Int): Long {
        var values =ContentValues()
        values.put(TEXT,text)
        values.put(PHASE,phase)
        return this.writableDatabase.insert(NOTICE_TABLE,null,values)
    }
    fun updateNotice(id:Int,text:String,phase:Int): Int {
        var values =ContentValues()
        values.put(TEXT,text)
        values.put(PHASE,phase)
        return this.writableDatabase.update(NOTICE_TABLE,values,"$ID=?", arrayOf("$id"))
    }
    fun deleteNotice(id:Int?){
       this.writableDatabase.delete(NOTICE_TABLE,"$ID=?", arrayOf("$id"))
    }


    fun getAllNotice():ArrayList<Text>{
        var list = ArrayList<Text>()
        var cursor = this.writableDatabase.query(NOTICE_TABLE, arrayOf(ID,TEXT,PHASE),null,null,null,null,null)
        if(cursor.moveToFirst()){
            do{
                var txt = Text()
                txt.id=cursor.getInt(0)
                txt.text=cursor.getString(1)
                txt.phase=cursor.getInt(2)
                list.add(txt)
            }while(cursor.moveToNext())
        }
        return list

    }
    fun selectNotice(phase:Int):ArrayList<Text>{
        var list = ArrayList<Text>()
        var cursor = this.writableDatabase.query(NOTICE_TABLE, arrayOf(ID,TEXT,PHASE),"$PHASE==$phase",null,null,null,null)
        if(cursor.moveToFirst()){
            do{
                var txt = Text()
                txt.id=cursor.getInt(0)
                txt.text=cursor.getString(1)
                txt.phase=cursor.getInt(2)
                list.add(txt)
            }while(cursor.moveToNext())
        }
        return list
    }
}
val Context.database: MyDatabaseOpenHelper
    get() = MyDatabaseOpenHelper.getInstance(getApplicationContext())