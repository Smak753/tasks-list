package com.example.spisok_del


import android.content.Context
import android.content.Intent
import android.os.Parcel
import android.os.Parcelable
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import kotlinx.android.synthetic.main.item.view.*

 class TextAdapter() : RecyclerView.Adapter<TextAdapter.TextViewHolder>(), Parcelable {
    var list:ArrayList<Text>?=null
    var context:Context?=null


    constructor(context: Context,phase:Int):this(){
        this.list= MyDatabaseOpenHelper.getInstance(context!!).selectNotice(phase)
        this.context=context
    }
    fun updateList(){
        this.list=MyDatabaseOpenHelper.getInstance(context!!).getAllNotice()
        notifyDataSetChanged()
    }
    override fun onBindViewHolder(holder: TextViewHolder, position: Int) {
            holder.text.text = list?.get(position)?.text

            holder.itemView.setOnClickListener({
                var Notice: Text = list!!.get(position)
                var intent = Intent(holder.itemView.context, Change::class.java)
                if(Notice.phase==1) {
                    intent.putExtra("btn", 0)
                }
                intent.putExtra("Notice", list?.get(position) as Parcelable)
                holder.itemView.context.startActivity(intent)
            })
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):TextViewHolder = TextViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item, parent, false))
    override fun getItemId(position: Int): Long = position.toLong()


    override fun getItemCount() = list!!.size //Кол-во записей

    class TextViewHolder(view: View):RecyclerView.ViewHolder(view){
        var text: TextView = view.viewItem

    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TextAdapter> {
        override fun createFromParcel(parcel: Parcel): TextAdapter {
            return TextAdapter()
        }

        override fun newArray(size: Int): Array<TextAdapter?> {
            return arrayOfNulls(size)
        }
    }

}